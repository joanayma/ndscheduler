FROM alpine:3.6
LABEL maintainer="Joan Aymà <joan.ayma@gmail.com>"

COPY files/ /
RUN apk --no-cache add git py-virtualenv py2-pip nginx supervisor && \
    pip install j2cli && \
    pip install -r /ndscheduler/simple_scheduler/requirements.txt && \
    mv /supervisord.conf /etc/supervisord.conf && \
    chmod +x /entrypoint.sh && \
    apk del git

ENV BASE_PATH="/"
VOLUME /datastore
ENTRYPOINT ["/entrypoint.sh"]
