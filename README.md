alpine ndscheduler
==================

Docker example with the Nextdoor Scheduler service.

Options
-------
`AUTH_HTPASSWD`: htpasswd authentication `user:$hash` line(s).
`BASE_PATH`: url subpath to use on nginx proxy.

Source
------
This is a fork of [a fork](https://github.com/micmejia/ndscheduler.git) of ndscheduler.
