#!/bin/sh -e

# trap
trap 'kill -SIGHUP `cat /tmp/supervisord.pid`' SIGUSR1
trap exit 0 SIGTERM

(! [ -z "$AUTH_HTPASSWD" ] && echo "$AUTH_PASSWD" > /.htpasswd) || [ -f /.htpasswd ] && rm /.htpasswd

j2 /jinja.d/nginx.conf.j2 > /etc/nginx/nginx.conf

supervisord -n -c /etc/supervisord.conf
